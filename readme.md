#### Use Debezium to stream Postgres changes to Kafka using Debezium' source connector (via Kafka).

1. `docker compose up`

2. Visit http://localhost:8080 (adminer) and enter the login details for Postgres: (`System=PostgreSQL`, `Server=postgres`, `Username=postgres`, `Password=postgres`, `Database=postgres`)

3. Create a heartbeat table for the sole purpose of allowing Debezium to make changes to it every heartbeat.interval.ms milliseconds. This will ensure that even in the case there haven’t been any changes in the database monitored Debezium will still periodically confirm the LSN and WAL will not cause out of storage issues
   ```sql
   CREATE TABLE IF NOT EXISTS debezium_heartbeat (
     id serial PRIMARY KEY,
     heartbeat_text VARCHAR (15)
   );
   ```

4. Create the `test` table:
   ```sql
   CREATE TABLE test (
     id SERIAL PRIMARY KEY,
     name VARCHAR (50),
     age SMALLINT
   );
   ```

5. Execute the following curl command to create the connector (note in the JSON payload that we only include the `public.test' table for change capture):
   ```shell
   curl --location --request POST 'http://localhost:8083/connectors' \
     --header 'Content-Type: application/json' \
     -d @connector-config-pgsql-source.json
   ```
   also, you can delete the connector with the following:
   ```shell
   curl -i -X DELETE localhost:8083/connectors/postgres-connector/
   ```

6. Insert a record into the `test` table: 
   ```sql
   INSERT INTO "test" ("name", "age")
   VALUES ('Paul McCartney', '79');
   ```

7. Visit http://localhost:9000/ and you should see `postgres.public.test` in the list of topics. You can view messages and confirm that a change event is in the topic for the insert.


8. Maybe do this, I dunno.
   ```shell
   curl --location --request POST 'http://localhost:8083/connectors' \
     --header 'Content-Type: application/json' \
     -d @connector-config-es-sink.json
   ```
   Results in:
   ```log
   Failed to find any class that implements Connector and which name matches io.confluent.connect.elasticsearch.ElasticsearchSinkConnector
   ```
   Need a custom dockerfile to install the ES connector into the image (see TODO links).


#### TODO:

* Output kafka topic data to ES using Debezium ES sink connector.
  * General steps we'll need to do:
    1. Start the ElasticSearch debezium connector
       * `curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @es-sink.json`
       * es-sink.json file: https://github.com/debezium/debezium-examples/blob/main/unwrap-smt/es-sink.json
    2. ~~Start ElasticSearch itself~~
       * DONE: this is already in our docker-compose file.
    3. Validate that changes are propagated to ES instance 
       * probably something like this: `curl 'http://localhost:9200/test/_search?pretty'`
  * https://debezium.io/blog/2018/01/17/streaming-to-elasticsearch/
    * https://github.com/debezium/debezium-examples/tree/main/unwrap-smt
  * https://stackoverflow.com/questions/65488883/trying-to-configure-debezium-image-for-elasticsearch-sink
  * https://github.com/debezium/debezium-examples/blob/main/unwrap-smt/debezium-jdbc-es/Dockerfile#L16
* Back services with volumes for data persistence.
* Determine if we can configure Debezium to fill the `before` field in the message payload. The kafka messages currently show only the after state. 
