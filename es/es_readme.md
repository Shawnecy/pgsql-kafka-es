8. Maybe do this, I dunno.
   ```shell
   curl --location --request POST 'http://localhost:8083/connectors' \
     --header 'Content-Type: application/json' \
     -d @connector-config-es-sink.json
   ```
   Results in:
   ```log
   Failed to find any class that implements Connector and which name matches io.confluent.connect.elasticsearch.ElasticsearchSinkConnector
   ```
   Need a custom dockerfile to install the ES connector into the image (see TODO links).


#### TODO:

* Output kafka topic data to ES using Debezium ES sink connector.
  * General steps we'll need to do:
    1. Start the ElasticSearch debezium connector
      * `curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @es-sink.json`
      * es-sink.json file: https://github.com/debezium/debezium-examples/blob/main/unwrap-smt/es-sink.json
    2. ~~Start ElasticSearch itself~~
      * DONE: this is already in our docker-compose file.
    3. Validate that changes are propagated to ES instance
      * probably something like this: `curl 'http://localhost:9200/test/_search?pretty'`
  * https://debezium.io/blog/2018/01/17/streaming-to-elasticsearch/
    * https://github.com/debezium/debezium-examples/tree/main/unwrap-smt
  * https://stackoverflow.com/questions/65488883/trying-to-configure-debezium-image-for-elasticsearch-sink
  * https://github.com/debezium/debezium-examples/blob/main/unwrap-smt/debezium-jdbc-es/Dockerfile#L16
* Back services with volumes for data persistence.
* Determine if we can configure Debezium to fill the `before` field in the message payload. The kafka messages currently show only the after state. 
